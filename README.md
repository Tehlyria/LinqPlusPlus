# Linq++

Header-only C++14 implementation of (most) of C#'s LINQ functions.

All functions are given `.begin()` and `.end()` iterators and most also accept a lambda for some transformation.

Where in C# one could write 

`.max(x => x.Property)` 

to get the maximum `Property`-Value, we have to return the actual value in the lambda like so: 

`.max(begin, end, [](const auto& elem) { return elem.Property; })`.

ReturnTypes of most methods is automatically deduced via template parameters. For the mathematical functions this is just some arithmetic type,
but is 100% generic for `.select()` or `.groupBy()`:

`.select(begin, end, [](const auto& elem) { return Foo { elem.Bar, elem.Bat, elem.Baz }; }`



## Implemented functions thus far:

- ### Mathematical
	- min
		- `min(Iter, Iter)` 
			- Returns the minimum element in the sequence.
		- `min(Iter, Iter, Func)`
			- Returns the minimum value described by the given function.
	- max 
		- `max(Iter, Iter)`
			- Returns the maximum element in the sequence.
		- `max(Iter, Iter, Func)`
			- Returns the maximum value described by the given function.
	- sum
		- `sum(Iter, Iter)`
			- Returns the sum of elements in the sequence.
		- `sum(Iter, Iter, Func)`
			- Returns the sum of values described by the given function.
	- count
		- `count(Iter, Iter)`
			- Returns the count of elements in the sequence.
		- `count(Iter, Iter, Pred)`
			- Returns the count of elements that match the given predicate.

- ### Filtering
	- where
		- `where(Iter, Iter, Pred)`
			- Returns all elements that match the given predicate.
	- skip
		- `skip(Iter, Iter, Num)`
			- Skips the first `Num` elements of the sequence and returns the rest.
	- skipWhile
		- `skipWhile(Iter, Iter, Pred)`
			- Skips elements (from the front) until one does not match the given predicate.
	- take
		- `take(Iter, Iter, Num)`
			- Takes the first `Num`elements of the sequence.
	- takeWhile
		- `takeWhile(Iter, Iter, Pred)`
			- Takes all elements (from the front) until one does not match the given predicate.
	- except
		- `except(Iter, Iter, Pred)`
			- Returns all elements except those that match the given predicate.
	- firstOrDefault
		- `firstOrDefault(Iter, Iter, Default)`
			- Returns the first element or the default if the sequence is empty.
		- `firstOrDefault(Iter, Iter, Pred, Default)`
			- Returns the first element that matches the given predicate or the default.
	- lastOrDefault
		- `lastOrDefault(Iter, Iter, Default)`
			- Returns the last element or the default if the sequence is empty.
		- `lastOrDefault(Iter, Iter, Pred, Default)`
			- Returns the last element that matches the given predicate or the default.
    - distinct
        - `distinct(Iter, Iter)`
            - Returns the sequence without duplicates.
        - `distinct(Iter, Iter, Func)`
            - Returns the sequence without duplicates on the property described by the given function.

- ### Existential
	- contains
		- `contains(Iter, Iter, Element)`
			- Returns true if the sequence contains the given element. Otherwise false.
	- any
		- `any(Iter, Iter, Pred)`
			- Returns whether there exists an element in the sequence that matches the given predicate.
	- all
		- `all(Iter, Iter, Pred)`
			- Returns whether all elements in the sequence match the given predicate.

- ### Ordering
	- orderBy
		- `orderBy(Iter, Iter, Compare)`
			- Returns an ordered sequence using the given comparator.

- ### Transformation
	- select
		- `select(Iter, Iter, Func)`
			- Returns a projected sequence using the given selector.
	- groupBy
		- `groupBy(Iter, Iter, KeyFunc)`
			- Returns a grouped collection using the property described by the given key-function as key.
		- `groupBy(Iter, Iter, KeyFunc, ResultFunc)`
			- Returns a grouped collection using the property described by the given key-function as key, and projecting its values onto the type described by the given result-function.
    - zip
        - `zip(FirstIter, FirstIter, SecondIter, SecondIter, Func)`
            - Returns a collection of elements constructed by the given function using the passed sequences.
   

