#include "gtest/gtest.h"

#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(NumericVector, distinct_numeric) {
  const auto dst = LINQpp::distinct(m_NonDistinct.begin(), m_NonDistinct.end());

  ASSERT_EQ(m_NonDistinct.size() - 1, dst.size());

  for (auto i = 1u; i <= 4; ++i) {
    ASSERT_EQ(i, dst.at(i - 1));
  }
}

TEST_F(StructVector, distinct_by_intvalue) {
  const auto dst =
      LINQpp::distinct(m_NonDistinct.begin(), m_NonDistinct.end(), [](const auto &elem) { return elem.IntValue; });

  ASSERT_EQ(m_NonDistinct.size() - 1, dst.size());

  for (auto i = 1u; i <= 2; ++i) {
    ASSERT_EQ(i, dst.at(i - 1).IntValue);
  }
}