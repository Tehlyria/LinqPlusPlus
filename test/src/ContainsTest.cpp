#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(NumericVector, contained) {
  const auto hasElem = LINQpp::contains(m_Vector.begin(), m_Vector.end(), 2);

  ASSERT_TRUE(hasElem);
}

TEST_F(EmptyVector, not_contained) {
  const auto hasElem = LINQpp::contains(m_Vector.begin(), m_Vector.end(), -5);

  ASSERT_FALSE(hasElem);
}

TEST_F(StructVector, struct_contained) {
  const auto hasElem = LINQpp::contains(m_Vector.begin(), m_Vector.end(), StructDummy{2});

  ASSERT_TRUE(hasElem);
}