#include "gtest/gtest.h"

#include "data/GroupVector.hpp"

#include "Linqpp.hpp"

TEST_F(GroupVector, groupby_intvalue) {
  const auto grp =
      LINQpp::groupBy(m_IntKeyVector.begin(), m_IntKeyVector.end(), [](const auto &elem) { return elem.IntValue; });

  ASSERT_EQ(2, grp.at(2).size());
  ASSERT_EQ(1, grp.at(3).size());
}

TEST_F(GroupVector, groupby_charvalue) {
  const auto grp =
      LINQpp::groupBy(m_CharKeyVector.begin(), m_CharKeyVector.end(), [](const auto &elem) { return elem.CharValue; });

  ASSERT_EQ(2, grp.at('a').size());
  ASSERT_EQ(1, grp.at('b').size());
}

TEST_F(GroupVector, groupby_into_struct) {
  struct Foo {
    int Value;
  };

  const auto grp = LINQpp::groupBy(
      m_CharKeyVector.begin(),
      m_CharKeyVector.end(),
      [](const auto &elem) { return elem.CharValue; },
      [](const auto &elem) { return Foo {elem.IntValue}; }
  );

  ASSERT_EQ(1, grp.at('b').size());
  ASSERT_EQ(2, grp.at('a').size());;
  using ElementType = typename std::iterator_traits<decltype(grp.at('a').begin())>::value_type;
  auto sameType = std::is_same<Foo, ElementType>::value;
  ASSERT_TRUE(sameType);
}