#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, except_empty) {
  const auto filtered = LINQpp::except(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val % 2 == 0; });

  ASSERT_EQ(0, filtered.size());
}

TEST_F(NumericVector, except_mod_2) {
  const auto filtered = LINQpp::except(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val % 2 == 0; });

  for (const auto &elem : filtered) {
    ASSERT_TRUE(elem % 2 != 0);
  }
}

TEST_F(StructVector, except_d2) {
  const auto
      filtered = LINQpp::except(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val.IntValue == 2; });

  for (const auto &elem : filtered) {
    ASSERT_TRUE(elem.IntValue != 2);
  }
}