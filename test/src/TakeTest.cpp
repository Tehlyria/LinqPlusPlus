#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, take_empty) {
  const auto filtered = LINQpp::take(m_Vector.begin(), m_Vector.end(), 10);

  ASSERT_EQ(0, filtered.size());
}

TEST_F(NumericVector, take_0) {
  const auto filtered = LINQpp::take(m_Vector.begin(), m_Vector.end(), 0);

  ASSERT_EQ(0, filtered.size());
}

TEST_F(NumericVector, take_3) {
  const auto filtered = LINQpp::take(m_Vector.begin(), m_Vector.end(), 3);

  ASSERT_EQ(3, filtered.size());

  for (auto i = 0u; i < filtered.size(); ++i) {
    ASSERT_EQ(m_Vector.at(i), filtered.at(i));
  }
}

TEST_F(NumericVector, take_all) {
  const auto filtered = LINQpp::take(m_Vector.begin(), m_Vector.end(), m_Vector.size());

  ASSERT_EQ(m_Vector.size(), filtered.size());

  for (auto i = 0u; i < filtered.size(); ++i) {
    ASSERT_EQ(m_Vector.at(i), filtered.at(i));
  }
}