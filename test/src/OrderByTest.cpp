#include "gtest/gtest.h"

#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(NumericVector, order_asc) {
  const auto ordered = LINQpp::orderBy(m_Vector.begin(),
                                       m_Vector.end(),
                                       [](const auto &first, const auto &second) { return first < second; });

  auto current = ordered.at(0);
  for (auto i = 1u; i < ordered.size(); ++i) {
    ASSERT_TRUE(current < ordered.at(i));
    current = ordered.at(i);
  }
}

TEST_F(NumericVector, order_desc) {
  const auto ordered = LINQpp::orderBy(m_Vector.begin(),
                                       m_Vector.end(),
                                       [](const auto &first, const auto &second) { return first > second; });

  auto current = ordered.at(0);
  for (auto i = 1u; i < ordered.size(); ++i) {
    ASSERT_TRUE(current > ordered.at(i));
    current = ordered.at(i);
  }
}

TEST_F(StructVector, order_struct) {
  const auto ordered = LINQpp::orderBy(m_Vector.begin(),
                                       m_Vector.end(),
                                       [](const auto &first, const auto &second) {
                                         return first.IntValue < second.IntValue;
                                       });

  auto current = ordered.at(0);
  for (auto i = 1u; i < ordered.size(); ++i) {
    ASSERT_TRUE(current.IntValue < ordered.at(i).IntValue);
    current = ordered.at(i);
  }
}