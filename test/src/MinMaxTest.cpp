#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, min_empty) {
  const auto min = LINQpp::min(m_Vector.begin(), m_Vector.end());

  ASSERT_EQ(0, min);
}

TEST_F(NumericVector, find_min_element) {
  const auto min = LINQpp::min(m_Vector.begin(), m_Vector.end());

  ASSERT_EQ(1, min);
}

TEST_F(StructVector, min_struct) {
  const auto min = LINQpp::min(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val.IntValue; });

  ASSERT_EQ(1, min);
}

TEST_F(EmptyVector, max_empty) {
  const auto max = LINQpp::max(m_Vector.begin(), m_Vector.end());

  ASSERT_EQ(0, max);
}

TEST_F(NumericVector, find_max_element) {
  const auto max = LINQpp::max(m_Vector.begin(), m_Vector.end());

  ASSERT_EQ(6, max);
}

TEST_F(StructVector, max_struct) {
  const auto max = LINQpp::max(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val.IntValue; });

  ASSERT_EQ(3, max);
}