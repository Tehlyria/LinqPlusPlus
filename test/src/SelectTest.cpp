#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, select_empty) {
  const auto selected = LINQpp::select(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val; });

  ASSERT_EQ(0, selected.size());
}

TEST_F(NumericVector, select_to_struct) {
  struct Foo {
    int Value;
  };

  const auto selected = LINQpp::select(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return Foo {val}; });

  ASSERT_EQ(m_Vector.size(), selected.size());

  for (auto i = 0u; i < m_Vector.size(); ++i) {
    ASSERT_EQ(m_Vector.at(i), selected.at(i).Value);
  }
}

TEST_F(StructVector, select_from_struct) {
  const auto selected = LINQpp::select(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val.IntValue; });

  ASSERT_EQ(m_Vector.size(), selected.size());

  for (auto i = 0u; i < m_Vector.size(); ++i) {
    ASSERT_EQ(m_Vector.at(i).IntValue, selected.at(i));
  }
}

TEST_F(NumericVector, select_same) {
  const auto selected = LINQpp::select(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val; });

  ASSERT_EQ(m_Vector.size(), selected.size());

  for (auto i = 0u; i < m_Vector.size(); ++i) {
    ASSERT_EQ(m_Vector.at(i), selected.at(i));
  }
}