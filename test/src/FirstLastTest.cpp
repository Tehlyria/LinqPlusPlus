#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(NumericVector, first) {
  const auto first = LINQpp::firstOrDefault(m_Vector.begin(), m_Vector.end());

  ASSERT_EQ(m_Vector.at(0), first);
}

TEST_F(EmptyVector, first_empty) {
  const auto first = LINQpp::firstOrDefault(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val == 5; });

  ASSERT_EQ(0, first);
}

TEST_F(StructVector, first_with_default) {
  const StructDummy defaultValue(1);
  const auto first = LINQpp::firstOrDefault(m_Vector.begin(),
                                            m_Vector.end(),
                                            [](const auto &val) { return val.IntValue < 1; },
                                            defaultValue);

  ASSERT_EQ(defaultValue.IntValue, first.IntValue);
}

TEST_F(StructVector, first_gt_2) {
  const auto first =
      LINQpp::firstOrDefault(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val.IntValue > 2; });

  ASSERT_EQ(3, first.IntValue);
}

TEST_F(NumericVector, last) {
  const auto last = LINQpp::lastOrDefault(m_Vector.begin(), m_Vector.end());

  ASSERT_EQ(m_Vector.at(m_Vector.size() - 1), last);
}

TEST_F(EmptyVector, last_empty) {
  const auto last = LINQpp::firstOrDefault(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val == 0; });

  ASSERT_EQ(0, last);
}

TEST_F(StructVector, last_with_default) {
  const StructDummy defaultValue(1);
  const auto last = LINQpp::firstOrDefault(m_Vector.begin(),
                                           m_Vector.end(),
                                           [](const StructDummy &val) { return val.IntValue < 1; },
                                           defaultValue);

  ASSERT_EQ(defaultValue.IntValue, last.IntValue);
}

TEST_F(StructVector, last_gt_2) {
  const auto
      last = LINQpp::firstOrDefault(m_Vector.begin(), m_Vector.end(), [](const StructDummy &val) { return val.IntValue > 2; });

  ASSERT_EQ(3, last.IntValue);
}