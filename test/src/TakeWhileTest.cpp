#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, takewhile_empty) {
  const auto filtered = LINQpp::takeWhile(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val < 5; });

  ASSERT_EQ(0, filtered.size());
}

TEST_F(NumericVector, takewhile_lt_1) {
  const auto filtered = LINQpp::takeWhile(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val < 1; });

  ASSERT_EQ(0, filtered.size());
}

TEST_F(NumericVector, takewhile_lt_4) {
  const auto sortedVec = LINQpp::orderBy(m_Vector.begin(),
                                         m_Vector.end(),
                                         [](const auto &current, const auto &smallest) { return current < smallest; });
  const auto filtered = LINQpp::takeWhile(sortedVec.begin(), sortedVec.end(), [](const auto &val) { return val < 4; });

  ASSERT_EQ(3, filtered.size());

  for (auto i = 0u; i < filtered.size(); ++i) {
    ASSERT_EQ(sortedVec.at(i), filtered.at(i));
  }
}

TEST_F(NumericVector, takewhile_all) {
  const auto filtered = LINQpp::takeWhile(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return true; });

  ASSERT_EQ(m_Vector.size(), filtered.size());

  for (auto i = 0u; i < filtered.size(); ++i) {
    ASSERT_EQ(m_Vector.at(i), filtered.at(i));
  }
}