#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, zip_empty) {
  std::vector<int> sec = {1, 2, 3};
  const auto zipped = LINQpp::zip(m_Vector.begin(),
                                  m_Vector.end(),
                                  sec.begin(),
                                  sec.end(),
                                  [](const auto &first, const auto &second) { return first + second; });

  ASSERT_EQ(0, zipped.size());
}

TEST_F(NumericVector, zip_double) {
  const auto zipped = LINQpp::zip(m_Vector.begin(),
                                  m_Vector.end(),
                                  m_Vector.begin(),
                                  m_Vector.end(),
                                  [](const auto &first, const auto &second) { return first + second; });

  ASSERT_EQ(m_Vector.size(), zipped.size());

  for (auto i = 0u; i < m_Vector.size(); ++i) {
    ASSERT_EQ(m_Vector.at(i) * 2, zipped.at(i));
  }
}

TEST_F(NumericVector, zip_different_types) {
  struct Foo {
    int FromFirst;
    float FromSecond;
  };

  auto sec = std::vector<float> {3.1415f, 2.7182f, 1.4142f};

  const auto zipped = LINQpp::zip(m_Vector.begin(),
                                  m_Vector.end(),
                                  sec.begin(),
                                  sec.end(),
                                  [](const auto &first, const auto &second) { return Foo {first, second}; });

  ASSERT_EQ(sec.size(), zipped.size());

  for (auto i = 0u; i < zipped.size(); ++i) {
    ASSERT_EQ(m_Vector.at(i), zipped.at(i).FromFirst);
    ASSERT_EQ(sec.at(i), zipped.at(i).FromSecond);
  }
}