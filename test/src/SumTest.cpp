#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, sum_empty) {
  const auto sum = LINQpp::sum(m_Vector.begin(), m_Vector.end());

  ASSERT_EQ(0, sum);
}

TEST_F(NumericVector, sum) {
  const auto sum = LINQpp::sum(m_Vector.begin(), m_Vector.end());

  ASSERT_EQ(21, sum);
}

TEST_F(NumericVector, sum_mod_2) {
  const auto
      sum = LINQpp::sum(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val % 2 == 0 ? val : 0; });

  ASSERT_EQ(12, sum);
}

TEST_F(StructVector, sum_value) {
  const auto sum = LINQpp::sum(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val.IntValue; });

  ASSERT_EQ(6, sum);
}