#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, count_empty) {
  const auto cnt = LINQpp::count(m_Vector.begin(), m_Vector.end());

  ASSERT_EQ(m_Vector.size(), cnt);
}

TEST_F(NumericVector, count) {
  const auto cnt = LINQpp::count(m_Vector.begin(), m_Vector.end());

  ASSERT_EQ(m_Vector.size(), cnt);
}

TEST_F(StructVector, count_mod_2) {
  const auto
      cnt = LINQpp::count(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val.IntValue % 2 == 0; });

  ASSERT_EQ(1, cnt);
}