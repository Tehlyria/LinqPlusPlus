#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, where_empty) {
  const auto filtered = LINQpp::where(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val % 2 == 0; });

  ASSERT_EQ(0, filtered.size());
}

TEST_F(NumericVector, where_gt_3) {
  const auto filtered = LINQpp::where(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val > 3; });

  for (const auto &elem : filtered) {
    ASSERT_TRUE(elem > 3);
  }
}

TEST_F(StructVector, where_lt_2) {
  const auto
      filtered = LINQpp::where(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val.IntValue < 2; });

  ASSERT_EQ(1, filtered.size());

  for (const auto &elem : filtered) {
    ASSERT_TRUE(elem.IntValue < 2);
  }
}