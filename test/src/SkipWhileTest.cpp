#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, skipwhile_empty) {
  const auto filtered = LINQpp::skipWhile(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val <= 3; });

  ASSERT_EQ(0, filtered.size());
}

TEST_F(NumericVector, skipwhile_lte_3) {
  auto sortedVec = LINQpp::orderBy(m_Vector.begin(),
                                   m_Vector.end(),
                                   [](const auto &current, const auto &smallest) { return current < smallest; });
  const auto filtered = LINQpp::skipWhile(sortedVec.begin(), sortedVec.end(), [](const auto &val) { return val <= 3; });

  ASSERT_EQ(3, filtered.size());

  for (auto i = 3u; i < filtered.size(); ++i) {
    ASSERT_EQ(m_Vector.at(i + 1), filtered.at(i));
  }
}

TEST_F(NumericVector, skipwhile_lte_6) {
  const auto filtered = LINQpp::skipWhile(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val <= 6; });

  ASSERT_EQ(0, filtered.size());
}

TEST_F(NumericVector, skipwhile_lt_1) {
  const auto filtered = LINQpp::skipWhile(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val <= 0; });

  ASSERT_EQ(m_Vector.size(), filtered.size());

}