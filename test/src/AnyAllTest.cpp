#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, any_empty) {
  const auto hasMod2 = LINQpp::any(m_Vector.begin(), m_Vector.end(), [](const int &val) { return val % 2 == 0; });

  ASSERT_FALSE(hasMod2);
}

TEST_F(NumericVector, any_mod_2) {
  const auto hasMod2 = LINQpp::any(m_Vector.begin(), m_Vector.end(), [](const int &val) { return val % 2 == 0; });

  ASSERT_TRUE(hasMod2);
}

TEST_F(NumericVector, any_ge_7) {
  const auto anyGreaterThan7 = LINQpp::any(m_Vector.begin(), m_Vector.end(), [](const int &val) { return val >= 7; });

  ASSERT_FALSE(anyGreaterThan7);
}

TEST_F(StructVector, any_eq_2) {
  const auto anyEq2 = LINQpp::any(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val.IntValue == 2; });

  ASSERT_TRUE(anyEq2);
}

TEST_F(EmptyVector, all_empty) {
  const auto allMod2 = LINQpp::all(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val % 2 == 0; });

  ASSERT_TRUE(allMod2);
}

TEST_F(NumericVector, all_lt_7) {
  const auto allLessThan7 = LINQpp::all(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val < 7; });

  ASSERT_TRUE(allLessThan7);
}

TEST_F(NumericVector, all_mod_2) {
  const auto allMod2 = LINQpp::all(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val % 2 == 0; });

  ASSERT_FALSE(allMod2);
}

TEST_F(StructVector, all_eq_1) {
  const auto allEq1 = LINQpp::all(m_Vector.begin(), m_Vector.end(), [](const auto &val) { return val.IntValue == 1; });

  ASSERT_FALSE(allEq1);
}