#include "gtest/gtest.h"

#include "data/EmptyVector.hpp"
#include "data/NumericVector.hpp"
#include "data/StructVector.hpp"

#include "Linqpp.hpp"

TEST_F(EmptyVector, skip_empty) {
  const auto filtered = LINQpp::skip(m_Vector.begin(), m_Vector.end(), 10);

  ASSERT_EQ(0, filtered.size());
}

TEST_F(NumericVector, skip_0) {
  const auto filtered = LINQpp::skip(m_Vector.begin(), m_Vector.end(), 0);

  ASSERT_EQ(m_Vector.size(), filtered.size());

  for (auto i = 0u; i < m_Vector.size(); ++i) {
    ASSERT_EQ(m_Vector.at(i), filtered.at(i));
  }
}

TEST_F(NumericVector, skip_3) {
  const auto filtered = LINQpp::skip(m_Vector.begin(), m_Vector.end(), 3);

  ASSERT_EQ(3, filtered.size());

  for (auto i = 3u; i < filtered.size(); ++i) {
    ASSERT_EQ(i + 1, filtered.at(i));
  }
}

TEST_F(NumericVector, skip_all) {
  const auto filtered = LINQpp::skip(m_Vector.begin(), m_Vector.end(), m_Vector.size());

  ASSERT_EQ(0, filtered.size());
}

TEST_F(StructVector, skip_first) {
  const auto filtered = LINQpp::skip(m_Vector.begin(), m_Vector.end(), 1);

  ASSERT_EQ(m_Vector.size() - 1, filtered.size());
}