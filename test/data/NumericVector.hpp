#pragma once

#include "gtest/gtest.h"

class NumericVector : public ::testing::Test {
protected:
  std::vector<int> m_Vector = {5, 3, 6, 4, 2, 1};
  std::vector<int> m_NonDistinct = {1, 1, 2, 3, 4};
};
