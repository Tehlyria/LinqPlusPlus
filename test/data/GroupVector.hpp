#pragma once

#include "gtest/gtest.h"

struct GroupDummy {
  int IntValue;
  char CharValue;

  friend auto operator==(const GroupDummy &lhs, const GroupDummy &rhs) {
    return lhs.IntValue == rhs.IntValue && lhs.CharValue == rhs.CharValue;
  }

  friend auto operator!=(const GroupDummy &lhs, const GroupDummy &rhs) {
    return !(lhs == rhs);
  }
};

class GroupVector : public ::testing::Test {
protected:
  std::vector<GroupDummy> m_IntKeyVector = {GroupDummy{2, 'a'}, GroupDummy{2, 'c'}, GroupDummy{3, 'b'}};
  std::vector<GroupDummy> m_CharKeyVector = {GroupDummy{3, 'a'}, GroupDummy{1, 'b'}, GroupDummy{2, 'a'}};
};