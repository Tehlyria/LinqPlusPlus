#pragma once

#include "gtest/gtest.h"

struct StructDummy {
  int IntValue;

  friend auto operator==(const StructDummy &lhs, const StructDummy &rhs) {
    return lhs.IntValue == rhs.IntValue;
  }

  friend auto operator!=(const StructDummy &lhs, const StructDummy &rhs) {
    return !(lhs == rhs);
  }

  StructDummy() = default;
  StructDummy(const int Val)
      : IntValue(Val) { }
};

class StructVector : public ::testing::Test {
protected:
  std::vector<StructDummy> m_Vector = {StructDummy{2}, StructDummy{1}, StructDummy{3}};
  std::vector<StructDummy> m_NonDistinct = {StructDummy {1}, StructDummy {1}, StructDummy {2}};
};