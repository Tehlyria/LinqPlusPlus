#pragma once

#include <functional>
#include <map>
#include <vector>

template<class TIter, class TKey, class TElement, class TResult>
class GroupedResult {
  std::map<TKey, std::vector<TResult>> Lookup;

  void addToGroup(TKey Key, const TResult &Elem) {
    auto Result = Lookup.find(Key);
    if (Result != Lookup.end()) {
      Result->second.push_back(Elem);
    } else {
      Lookup.insert(std::make_pair(Key, std::vector<TResult>{Elem}));
    }
  }

  void createLookup(TIter Begin,
                    TIter End,
                    std::function<TKey(const TElement &)> KeySelector,
                    std::function<TResult(const TElement &)> ResultSelector) {
    for (; Begin != End; ++Begin) {
      addToGroup(KeySelector(*Begin), ResultSelector(*Begin));
    }
  }

public:
  auto begin() {
    return Lookup.begin();
  }

  auto begin() const {
    return Lookup.begin();
  }

  auto end() {
    return Lookup.end();
  }

  auto end() const {
    return Lookup.end();
  }

public:
  size_t size() const {
    return Lookup.size();
  }

  std::vector<TResult> at(TKey Key) const {
    return Lookup.at(Key);
  }

public:
  GroupedResult(TIter Begin,
                TIter End,
                std::function<TKey(const TElement &)> KeySelector,
                std::function<TResult(const TElement &)> ResultSelector) {
    createLookup(Begin, End, KeySelector, ResultSelector);
  }

  ~GroupedResult() = default;
};