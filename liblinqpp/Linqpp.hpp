#pragma once

#include "Enumerable.hpp"
#include "GroupedResult.hpp"
#include "Types.hpp"

namespace LINQpp {
template<class TIter, class TFunc = IdentityFunc>
auto max(TIter Begin, TIter End, TFunc TargetProperty = TFunc()) {
  return Query::From(Begin, End).max(TargetProperty);
}

template<class TIter, class TFunc = IdentityFunc>
auto min(TIter Begin, TIter End, TFunc TargetProperty = TFunc()) {
  return Query::From(Begin, End).min(TargetProperty);
}

template<class TIter, class TFunc = IdentityFunc>
auto sum(TIter Begin, TIter End, TFunc TargetProperty = TFunc()) {
  return Query::From(Begin, End).sum(TargetProperty);
}

template<class TIter, class TPredicate = TrueFunc>
auto count(TIter Begin, TIter End, TPredicate Pred = TPredicate()) {
  return Query::From(Begin, End).count(Pred);
}

template<class TIter, class TPredicate>
auto where(TIter Begin, TIter End, TPredicate Pred) {
  return Query::From(Begin, End).where(Pred);
}

template<class TIter, class TPredicate = TrueFunc>
auto firstOrDefault(TIter Begin,
                    TIter End,
                    TPredicate Pred = TPredicate(),
                    ElementType<TIter> DefaultValue = ElementType<TIter>()) {
  return Query::From(Begin, End).firstOrDefault(Pred, DefaultValue);
}

template<class TIter, class TPredicate = TrueFunc>
auto lastOrDefault(TIter Begin,
                   TIter End,
                   TPredicate Pred = TPredicate(),
                   ElementType<TIter> DefaultValue = ElementType<TIter>()) {
  return Query::From(Begin, End).lastOrDefault(Pred, DefaultValue);
}

template<class TIter>
auto contains(TIter Begin, TIter End, ElementType<TIter> Value) {
  return Query::From(Begin, End).contains(Value);
}

template<class TIter, class TPredicate>
auto except(TIter Begin, TIter End, TPredicate Pred) {
  return Query::From(Begin, End).except(Pred);
}

template<class TIter, class TSortPredicate>
auto orderBy(TIter Begin, TIter End, TSortPredicate Comparator) {
  return Query::From(Begin, End).orderBy(Comparator);
}

template<class TIter, class TPredicate>
auto any(TIter Begin, TIter End, TPredicate Pred) {
  return Query::From(Begin, End).any(Pred);
}

template<class TIter, class TPredicate>
auto all(TIter Begin, TIter End, TPredicate Pred) {
  return Query::From(Begin, End).all(Pred);
}

template<class TIter>
auto skip(TIter Begin, TIter End, size_t Amount) {
  return Query::From(Begin, End).skip(Amount);
}

template<class TIter, class TPredicate>
auto skipWhile(TIter Begin, TIter End, TPredicate Pred) {
  return Query::From(Begin, End).skipWhile(Pred);
}

template<class TIter>
auto take(TIter Begin, TIter End, size_t Amount) {
  return Query::From(Begin, End).take(Amount);
}

template<class TIter, class TPredicate>
auto takeWhile(TIter Begin, TIter End, TPredicate Pred) {
  return Query::From(Begin, End).takeWhile(Pred);
}

template<class TIter, class TFunc>
auto select(TIter Begin, TIter End, TFunc Selector) {
  return Query::From(Begin, End).select(Selector);
}

template<class TIter, class TKeyFunc, class TResultFunc = IdentityFunc>
auto groupBy(TIter Begin, TIter End, TKeyFunc KeySelector, TResultFunc ResultSelector = TResultFunc()) {
  return GroupedResult<TIter, ReturnType<TIter, TKeyFunc>, ElementType<TIter>, ReturnType<TIter, TResultFunc>>(Begin,
                                                                                                               End,
                                                                                                               KeySelector,
                                                                                                               ResultSelector);
}

template<class TIter, class TFunc = IdentityFunc>
auto distinct(TIter Begin, TIter End, TFunc Selector = TFunc()) {
  return Query::From(Begin, End).distinct(Selector);
}

template<class TFirstIter, class TSecondIter, class TFunc>
auto zip(TFirstIter BeginFirst, TFirstIter EndFirst, TSecondIter BeginSecond, TSecondIter EndSecond, TFunc Selector) {
  return Query::From(BeginFirst, EndFirst).zip(BeginSecond, EndSecond, Selector);
}
}