#pragma once

#include <functional>

template<class TIter>
using ElementType = typename std::iterator_traits<TIter>::value_type;

template<class TIter, class TFunc>
using ReturnType = std::result_of_t<TFunc(ElementType<TIter> &)>;

template<class TIter>
using SortPredicate = std::function<bool(const ElementType<TIter> &, const ElementType<TIter> &)>;

struct TrueFunc {
  template<class TIter>
  constexpr auto operator()(TIter &&Elem) const noexcept {
    return true;
  }
};

struct IdentityFunc {
  template<class TIter>
  constexpr auto operator()(TIter &&Elem) const noexcept {
    return std::forward<TIter>(Elem);
  }
};