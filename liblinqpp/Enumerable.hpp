#pragma once

#include "Types.hpp"

#include <algorithm>
#include <functional>

class Query;

namespace LINQpp {
template<class TIter>
class Enumerable {
  friend class Query;

  TIter Begin;
  TIter End;

  Enumerable(TIter BeginIter, TIter EndIter)
      : Begin(BeginIter), End(EndIter) {}

public:
  template<class TFunc>
  auto max(TFunc TargetProperty);

  template<class TFunc>
  auto min(TFunc TargetProperty);

  template<class TFunc = IdentityFunc>
  auto sum(TFunc TargetProperty = TFunc());

  template<class TPredicate>
  auto count(TPredicate Pred);

  template<class TPredicate>
  auto where(TPredicate Pred);
  template<class TPredicate>
  auto except(TPredicate Pred);
  template<class TSortPredicate>
  auto orderBy(TSortPredicate Comparator);
  auto skip(size_t Amount);
  template<class TPredicate>
  auto skipWhile(TPredicate Pred);
  auto take(size_t Amount);
  template<class TPredicate>
  auto takeWhile(TPredicate Pred);

  template<class TFunc = IdentityFunc>
  auto distinct(TFunc Selector = TFunc());

  template<class TFunc>
  auto select(TFunc Selector);

  template<class TPredicate>
  auto firstOrDefault(TPredicate Pred = TrueFunc(),
                                    ElementType<TIter> DefaultValue = ElementType<TIter>());
  template<class TPredicate>
  auto lastOrDefault(TPredicate Pred = TrueFunc(),
                                   ElementType<TIter> DefaultValue = ElementType<TIter>());

  bool contains(ElementType<TIter> Value);

  template<class TPredicate>
  bool any(TPredicate Pred);

  template<class TPredicate>
  bool all(TPredicate Pred);

  template<class TSecondIter, class TFunc>
  auto zip(TSecondIter SecondBegin, TSecondIter SecondEnd, TFunc Selector);
};

class Query {
public:
  template<class TIter>
  static Enumerable<TIter> From(TIter Begin, TIter End) {
    return Enumerable<TIter>(Begin, End);
  }
};

template<class TIter>
template<class TFunc>
auto Enumerable<TIter>::max(TFunc TargetProperty) {
  static_assert(std::is_arithmetic<ReturnType<TIter, TFunc>>::value, "Not an arithmetic type!");

  if (Begin == End)
    return ReturnType<TIter, TFunc>();

  auto Result = TargetProperty(*Begin);
  auto Iter = ++Begin;

  for (; Iter != End; ++Iter) {
    if (TargetProperty(*Iter) > Result) {
      Result = TargetProperty(*Iter);
    }
  }

  return Result;
}

template<class TIter>
template<class TFunc>
auto Enumerable<TIter>::min(TFunc TargetProperty) {
  static_assert(std::is_arithmetic<ReturnType<TIter, TFunc>>::value, "Not an arithmetic type!");

  if (Begin == End)
    return ReturnType<TIter, TFunc>();

  auto Result = TargetProperty(*Begin);
  auto Iter = ++Begin;

  for (; Iter != End; ++Iter) {
    if (TargetProperty(*Iter) < Result) {
      Result = TargetProperty(*Iter);
    }
  }

  return Result;
}

template<class TIter>
template<class TFunc>
auto Enumerable<TIter>::sum(TFunc TargetProperty) {
  static_assert(std::is_arithmetic<ReturnType<TIter, TFunc>>::value, "Not an arithmetic type!");

  ReturnType<TIter, TFunc> Result{};
  for (; Begin != End; ++Begin) {
    Result += TargetProperty(*Begin);
  }

  return Result;
};

template<class TIter>
template<class TPredicate>
auto Enumerable<TIter>::count(TPredicate Pred) {
  return std::count_if(Begin, End, Pred);
}

template<class TIter>
template<class TPredicate>
auto Enumerable<TIter>::where(TPredicate Pred) {
  std::vector<ElementType<TIter>> Result;

  for (; Begin != End; ++Begin) {
    if (Pred(*Begin)) {
      Result.emplace_back(*Begin);
    }
  }

  return Result;
}

template<class TIter>
template<class TPredicate>
auto Enumerable<TIter>::except(TPredicate Pred) {
  std::vector<ElementType<TIter>> Result;

  for (; Begin != End; ++Begin) {
    if (Pred(*Begin)) {
      continue;
    }

    Result.emplace_back(*Begin);
  }

  return Result;
}

template<class TIter>
template<class TSortPredicate>
auto Enumerable<TIter>::orderBy(TSortPredicate Comparator) {
  std::vector<ElementType<TIter>> Result(Begin, End);
  std::sort(Result.begin(), Result.end(), Comparator);

  return Result;
}

template<class TIter>
auto Enumerable<TIter>::skip(size_t Amount) {
  for (auto i = 0u; i < Amount && Begin != End; ++i) {
    ++Begin;
  }

  return std::vector<ElementType<TIter>>(Begin, End);
}

template<class TIter>
template<class TPredicate>
auto Enumerable<TIter>::skipWhile(TPredicate Pred) {
  while (Begin != End && Pred(*Begin)) {
    ++Begin;
  }

  return Begin == End ? std::vector<ElementType<TIter>>() : std::vector<ElementType<TIter>>(Begin, End);
}

template<class TIter>
auto Enumerable<TIter>::take(size_t Amount) {
  std::vector<ElementType<TIter>> Result;
  for (auto i = 0u; i < Amount && Begin != End; ++i) {
    Result.emplace_back(*Begin);
    ++Begin;
  }

  return Result;
}

template<class TIter>
template<class TPredicate>
auto Enumerable<TIter>::takeWhile(TPredicate Pred) {
  std::vector<ElementType<TIter>> Result;
  for (; Begin != End; ++Begin) {
    if (Pred(*Begin)) {
      Result.emplace_back(*Begin);
    } else {
      break;
    }
  }

  return Result;
}

template<class TIter>
template<class TFunc>
auto Enumerable<TIter>::distinct(TFunc Selector) {
  std::set<ReturnType<TIter, TFunc>> Added;
  std::vector<ElementType<TIter>> Result;

  for (; Begin != End; ++Begin) {
    auto Res = Added.insert(Selector(*Begin));
    if (Res.second) {
      Result.emplace_back(*Begin);
    }
  }

  return Result;
}

template<class TIter>
template<class TFunc>
auto Enumerable<TIter>::select(TFunc Selector) {
  std::vector<ReturnType<TIter, TFunc>> Result;
  for (; Begin != End; ++Begin) {
    Result.emplace_back(Selector(*Begin));
  }

  return Result;
}

template<class TIter>
template<class TPredicate>
auto Enumerable<TIter>::firstOrDefault(TPredicate Pred, ElementType<TIter> DefaultValue) {
  for (; Begin != End; ++Begin) {
    if (Pred(*Begin)) {
      return *Begin;
    }
  }

  return DefaultValue;
}

template<class TIter>
template<class TPredicate>
auto Enumerable<TIter>::lastOrDefault(TPredicate Pred, ElementType<TIter> DefaultValue) {
  auto Iter = --End;
  for (; Iter != Begin; --Iter) {
    if (Pred(*Iter)) {
      return *Iter;
    }
  }

  return DefaultValue;
}

template<class TIter>
bool Enumerable<TIter>::contains(ElementType<TIter> Value) {
  for (; Begin != End; ++Begin) {
    if (*Begin == Value) {
      return true;
    }
  }

  return false;
}

template<class TIter>
template<class TPredicate>
bool Enumerable<TIter>::any(TPredicate Pred) {
  for (; Begin != End; ++Begin) {
    if (Pred(*Begin)) {
      return true;
    }
  }

  return false;
}

template<class TIter>
template<class TPredicate>
bool Enumerable<TIter>::all(TPredicate Pred) {
  for (; Begin != End; ++Begin) {
    if (!Pred(*Begin)) {
      return false;
    }
  }

  return true;
}

template<class TIter>
template<class TSecondIter, class TFunc>
auto Enumerable<TIter>::zip(TSecondIter SecondBegin, TSecondIter SecondEnd, TFunc Selector) {
  using RetType = std::result_of_t<TFunc(ElementType<TIter> & , ElementType<TSecondIter> & )>;

  std::vector<RetType> Result;

  for (; Begin != End && SecondBegin != SecondEnd; ++Begin, ++SecondBegin) {
    Result.emplace_back(Selector(*Begin, *SecondBegin));
  }

  return Result;
}
};
